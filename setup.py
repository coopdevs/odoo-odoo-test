from setuptools import setup

setup(
    name="odoo_odoo_test",
    version="0.0.2",
    author="Coopdevs",
    description="Odoo customizations for odoo-test",
    license="GPL",
    packages=['odoo/addons/odoo_odoo_test'],
    classifiers=[],
)
